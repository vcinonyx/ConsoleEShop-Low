﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConsoleEshopLowLib.DB;
using ConsoleEshopLowLib.Models;
using ConsoleEshopLowLib.Users;
using ConsoleEshopLowLib.Enums;

namespace ConsoleEshopLowLib.Services
{
    public class OrderService
    {
        private readonly OrdersDb _ordersDb;

        public OrderService()
        {
            _ordersDb = new OrdersDb();
        }

        public int AddOrder(List<(Product, int)> products, IUser customer)
        {
            return _ordersDb.AddOrder(products, customer);
        }

        public bool ContainsOrder(int id)
        {
            return _ordersDb.ContainsOrder(id);
        }

        public Order GetOrder(int id)
        {
            return _ordersDb.GetOrder(id);
        }

        public Order GetOrder(int id, IUser user)
        {
            return _ordersDb.GetOrder(id, user);
        }

        public List<Order> GetOrders()
        {
            return _ordersDb.GetItems();
        }

        public void SetStatus(Order order, OrderStatus status)
        {
            order.Status = status;
        }
    }
}
