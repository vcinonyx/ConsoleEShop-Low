﻿using ConsoleEshopLowLib.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEshopLowLib.Users
{
    public class Guest : IUser
    {
        public int Id { get; }

        public UserRole Role { get; } = UserRole.Guest;
    }
}
