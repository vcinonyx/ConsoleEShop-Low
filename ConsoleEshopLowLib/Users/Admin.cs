﻿using System;
using System.Collections.Generic;
using System.Text;
using ConsoleEshopLowLib.Enums;


namespace ConsoleEshopLowLib.Users
{
    public class Admin : IUser
    {
        private static int _counterId;
        public int Id { get; }
        public UserRole Role { get; } = UserRole.Admin;
        public string Name { get; }
        public string Surname { get; }

        public Admin(string name, string surname)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentException("Name can't be empty or null", nameof(name));

            if (string.IsNullOrEmpty(surname))
                throw new ArgumentException("Surname can't be empty or null", nameof(surname));

            Id = --_counterId;
            Name = name;
            Surname = surname;
        }
    }
}
