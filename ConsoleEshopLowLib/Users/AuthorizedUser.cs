﻿using System;
using System.Collections.Generic;
using ConsoleEshopLowLib.Enums;

namespace ConsoleEshopLowLib.Users
{
    public class AuthorizedUser : IUser
    {
        private int _counterId = 1;
        public UserRole Role { get; } = UserRole.AuthorizedUser;
        public int Id { get; }
        public string Name { get; private set; }
        public string Surname { get; private set; }

        
        public AuthorizedUser()
        {
            Id = ++_counterId;
        }


        public AuthorizedUser(string name, string surname)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentException("Name can't be empty or null", nameof(name));

            if (string.IsNullOrEmpty(surname))
                throw new ArgumentException("Surname can't be empty or null", nameof(surname));

            Id = ++_counterId;
            Name = name;
            Surname = surname;
        }


        public void Edit(string name, string surname)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentException("Name can't be empty or null", nameof(name));

            if (string.IsNullOrEmpty(surname))
                throw new ArgumentException("Surname can't be empty or null", nameof(surname));

            Name = name;
            Surname = surname;
        }
    }
}
