﻿using System;
using System.Collections.Generic;
using System.Text;
using ConsoleEshopLowLib.Models;
using ConsoleEshopLowLib.Users;
using System.Linq;

namespace ConsoleEshopLowLib.DB
{
    public class OrdersDb 
    {
        private readonly List<Order> _orders = new List<Order>();
        
        public List<Order> GetItems() => _orders;

        public int AddOrder(IEnumerable<(Product product, int quantity)> products, IUser user) 
        {
            if (products == null)
                throw new ArgumentNullException(nameof(products));

            if (user == null)
                throw new ArgumentNullException(nameof(user));

            var order = new Order(products, user);
            _orders.Add(order);

            return order.Id;
        }

        public bool ContainsOrder(int Id)
        {
            return _orders.Exists(x => x.Id == Id);
        }

        public Order GetOrder(int Id)
        {
            return _orders.FirstOrDefault(x => x.Id == Id);
        }

        public Order GetOrder(int Id, IUser user)
        {
            return _orders.First(x => x.Id == Id && x.CustomerId == user.Id);
        }
    }
}
