﻿using System;
using System.Collections.Generic;
using ConsoleEshopLowLib.Users;
using System.Linq;

namespace ConsoleEshopLowLib.DB
{
    public class UsersDb
    {
        private readonly List<IUser> _users;
        private readonly List<(int id, string username, string password)> _usersinfo;

        public UsersDb()
        {
            var admin = new Admin("admin", "admin");
            _users = new List<IUser> { admin };
            _usersinfo = new List<(int id, string username, string password)> { (admin.Id, "admin", "admin") };
        }

        public bool AddUser(string username, string password, IUser user)
        {
            if (string.IsNullOrEmpty(username))
                throw new ArgumentException("Username can't be empty or null", nameof(username));

            if (string.IsNullOrEmpty(password))
                throw new ArgumentException("Password can't be empty or null", nameof(password));

            if (user is null)
                throw new ArgumentNullException("User's object can't be null", nameof(user));

            if (_usersinfo.Select(x => x.username).Contains(username))
                return false;

            _users.Add(user);
            _usersinfo.Add((user.Id, username, password));

            return true;
        }

        public bool AddUser(string username, string password)
        {
            if (string.IsNullOrEmpty(username))
                throw new ArgumentException("Username can't be empty or null", nameof(username));

            if (string.IsNullOrEmpty(password))
                throw new ArgumentException("Password can't be empty or null", nameof(password));

            if (_usersinfo.Select(x => x.username).Contains(username))
                return false;

            var user = new AuthorizedUser();
            _users.Add(user);
            _usersinfo.Add((user.Id, username, password));

            return true;
        }

        public IUser GetUser(string username, string password)
        {
            if (_usersinfo.Select(x => x.username).Contains(username))
            {
                var userinfo = _usersinfo.First(x => x.username == username);
                var user = (userinfo.password == password) ? _users.FirstOrDefault(x => x.Id == userinfo.id) : null;
                return user;
            }

            return null;
        }
        public List<AuthorizedUser> GetAuthorizedUsers()
        {
            var authorizedUsers = new List<AuthorizedUser>();
            foreach (var user in _users)
            {
                if (user is AuthorizedUser authorized)
                {
                    authorizedUsers.Add(authorized);
                }
            }

            return authorizedUsers;
        }
    }
}
