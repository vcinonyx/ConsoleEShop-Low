﻿using System;
using System.Collections.Generic;
using System.Text;
using ConsoleEshopLowLib;
using ConsoleEshopLowLib.DB;
using ConsoleEshopLowLib.Enums;
using ConsoleEshopLowLib.Models;
using ConsoleEshopLowLib.Services;
using ConsoleEshopLowLib.Users;

namespace ConsoleAppEshop.ProcessHandlers
{
    public class UserProcessHandler : IProcessHandler
    {
        private readonly UserService _userService;
        private readonly ProductService _productService;
        private readonly OrderService _orderService;

        public UserProcessHandler(UserService userService, ProductService productService, OrderService orderService)
        {
            _userService = userService;
            _productService = productService;
            _orderService = orderService;
        }

        public void ProcessRequest(int i)
        {
            switch (i)
            {
                case 3:
                    CreateOrder();
                    break;
                case 4:
                    ProcessOrder();
                    break;
                case 5:
                    ShowOrders();
                    break;
                case 6:
                    SetReceivedStatus();
                    break;
                case 7:
                    ChangeInfo();
                    break;
                case 8:
                    _userService.Logout();
                    break;
                default:
                    Console.WriteLine("Invalid input, you need to type number");
                    break;
            }
        }

        private void ShowOrders()
        {
            var orders = _orderService.GetOrders();
            foreach (var order in orders)
            {
                if (order.CustomerId == _userService.CurrentUser.Id)
                {
                    Console.WriteLine(order.ToString());
                }
            }
        }

        private void ChangeInfo()
        {
            Console.WriteLine("Enter your name:");
            var name = Console.ReadLine();
            Console.WriteLine("Enter your surname:");
            var surname = Console.ReadLine();
            if (_userService.CurrentUser is AuthorizedUser authorized)
            {
                authorized.Edit(name, surname);
            }
            else
            {
                //throw new WrongUserException();
            }
        }

        private void SetReceivedStatus()
        {
            Console.WriteLine("Enter your order id:");
            if (!int.TryParse(Console.ReadLine(), out var orderId))
                throw new ArgumentException("You did not enter a number");
            
            var order = _orderService.GetOrder(orderId, _userService.CurrentUser);
            if (order is null) 
            {     
                Console.WriteLine("No such order or it is not your order"); 
            }
            else
            {
                if (order.Status != OrderStatus.Sent && order.Status != OrderStatus.PaymentReceived && order.Status != OrderStatus.New && order.Status != OrderStatus.InProgress) { Console.WriteLine("Order is already closed or confirmed"); return; }
                _orderService.SetStatus(order, OrderStatus.Received);
                Console.WriteLine("Order status set");
            }
        }

        private void CreateOrder()
        {
            Console.WriteLine("How many products do you want to order?");
            if (!int.TryParse(Console.ReadLine(), out var number)) 
                throw new ArgumentException("You did not enter a number");
            
            var productsList = new List<(Product Product, int Quantity)>();
            for (var i = 0; i < number; i++)
            {
                Console.WriteLine("Enter product name:");
                var productName = Console.ReadLine();
                if (_productService.ContainsProduct(productName))
                {
                    Console.WriteLine("Enter the quantity of this product in the order");
                    if (!int.TryParse(Console.ReadLine(), out var quantity)) throw new ArgumentException("You did not enter a number");
                    if (productsList.Exists(x => x.Product.Name == productName))
                    {
                        var existProduct = productsList.Find(x => x.Product.Name == productName);
                        productsList.Remove(existProduct);
                        existProduct.Quantity += quantity;
                        productsList.Add(existProduct);
                    }
                    else
                    {
                        productsList.Add((_productService.GetProduct(productName).product, quantity));
                    }
                }
                else Console.WriteLine("There are no product with this name");
            }

            try
            {
                Console.WriteLine($"Order created successfully. Order id: {_orderService.AddOrder(productsList, _userService.CurrentUser)}");
            }
            catch
            {
                Console.WriteLine("An error occurred while creating");
            }
        }

        private void ProcessOrder()
        {
            Console.WriteLine("Enter your order id:");
            if (!int.TryParse(Console.ReadLine(), out var orderId)) throw new ArgumentException("You did not enter a number");
            var order = _orderService.GetOrder(orderId, _userService.CurrentUser);
            if (order is null) Console.WriteLine("No such order or it is not your order");
            else
            {
                if (order.Status != OrderStatus.New) { Console.WriteLine("Order is already in progress, closed or confirmed"); return; }
                if (!_productService.CheckInStock(order.GetProducts))
                {
                    Console.WriteLine("Sorry, there are currently not enough products you order.");
                }
                else
                {
                    Console.WriteLine(
                        $"You need to pay {order.TotalSum.ToString()}. Do you want to confirm payment?(0 - no, 1 - yes)");
                    if (!int.TryParse(Console.ReadLine(), out var choice)) throw new ArgumentException("You did not enter a number");
                    switch (choice)
                    {
                        case 0:
                            _orderService.SetStatus(order, OrderStatus.CanceledByUser);
                            Console.WriteLine($"Order #{orderId} canceled");
                            break;
                        case 1:
                            _productService.ProductsReservation(order.GetProducts);
                            _orderService.SetStatus(order, OrderStatus.InProgress);
                            Console.WriteLine($"Order #{orderId} is awaiting confirmation of payment by admin. The products you ordered are reserved");
                            break;
                        default:
                            Console.WriteLine("Invalid number, only 0 or 1 allowed");
                            break;
                    }
                }
            }
        }
    }
}