﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleAppEshop.ProcessHandlers
{
    public interface IProcessHandler
    {
        void ProcessRequest(int i);
    }
}
